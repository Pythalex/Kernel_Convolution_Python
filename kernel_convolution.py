from PIL import Image
from statistics import mean
from datetime import datetime

def timestamp():
    time = datetime.now()
    return "{}:{}:{}".format(
        "0" + str(time.hour) if time.hour < 10 else time.hour,
        "0" + str(time.minute) if time.minute < 10 else time.minute,
        "0" + str(time.second) if time.second < 10 else time.second
    )

def valid_position(x, y, w, h):
    """
    Indicates whether x is in [0;w] and y is in [0;h]
    """
    return x >= 0 and x < w and y >= 0 and y < h

def compute_pixel(pixels, matrix, x, y, w, h, weight):
    """
    Calculates the output pixel value at x y.
    """

    # computes pixels
    sum = [0, 0, 0]
    for dx in (-1, 0, 1):
        for dy in (-1, 0, 1):
            if valid_position(x + dx, y + dy, w, h):

                shades = pixels[x + dx, y + dy]
                # linear algebra
                for i in range(3):
                    # for each channel
                    sum[i] += shades[i] * matrix[dy + 1][dx + 1]

    # average
    for i in range(3):
        sum[i] = int(sum[i] / weight)
    return sum

def compute_output_image(image, matrix):
    """
    Applies the matrix filter to the image input and 
    returns the new pixel array.
    """

    pix = image.load()
    w, h = image.size

    weight = sum(matrix[0]) + sum(matrix[1]) + sum(matrix[2])

    new_pix = []

    # for each pixel, compute the new value
    for y in range(h):
        for x in range(w):
            shades = compute_pixel(pix, matrix, x, y, w, h, weight)
            new_pix.append(tuple(shades))

    return new_pix

def apply_filter(image, matrix):
    """
    Applies the given filter to the image and returns the new image.
    """
    new_pix = compute_output_image(image, matrix)
    copy = image.copy()
    copy.putdata(new_pix)
    return copy


# some filters
BOX_BLUR = [[1, 1, 1], [1, 1, 1], [1, 1, 1]]
GAUSSIAN_BLUR = [[1, 2, 1], [2, 4, 2], [1, 2, 1]]
SHARPEN = [[0, -1, 0], [-1, 5, -1], [0, -1, 0]]

if __name__ == '__main__':

    input = Image.open("input.png")

    print("[{}] Compute bblur".format(timestamp()))
    output_bblur = apply_filter(input, BOX_BLUR)
    print("[{}] Compute gblur".format(timestamp()))
    output_gblur = apply_filter(input, GAUSSIAN_BLUR)
    print("[{}] Compute sharpen".format(timestamp()))
    output_sharpen = apply_filter(input, SHARPEN)

    print("[{}] Saving outputs".format(timestamp()))
    output_bblur.save("bblur.png")
    output_gblur.save("gblur.png")
    output_sharpen.save("sharpen.png")

    print("[{}] Done".format(timestamp()))